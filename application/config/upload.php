<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config = array(
    'uploadPath' => "assets/img/avatar",
    'allowedTypes' => "jpg|jpeg|png",
    'maxSize' => 10000000,
    'fileName' => $_SESSION['adminlogged_in']['email'].".jpg"
);

?>