<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Welcome to CodeIgniter</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style type="text/css">
        .emptyrow{
            height: 20px;
            width: 100%;
        }
        .datepicker td, .datepicker th {
            width: 2em;
            height: 2em;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h1>Academic Details</h1>
        </div>
    </div>
    <div class="emptyrow"></div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <form action="<?= base_url(); ?>register" method="POST">
                <div class="row">
                    <div class="col-4 text-right">
                        Full Name: 
                    </div>
                    <div class="col-8">
                        <input type="text" name="fullname" class="form-control" value="<?php echo $userdata['fullname'] ?>" readonly>
                    </div>
                </div>
                <div class="emptyrow"></div>

                <input type="hidden" name="usertype" class="form-control" value="<?php echo $userdata['usertype'] ?>">                
                <input type="hidden" name="gender" class="form-control" value="<?php echo $userdata['gender'] ?>">           
                <input type="hidden" name="treatment" class="form-control" value="<?php echo $userdata['treatment'] ?>">                
                <input type="hidden" name="dob" class="form-control" value="<?php echo $userdata['dob'] ?>">
                <input type="hidden" name="fname" class="form-control" value="<?php echo $userdata['fname'] ?>">
                <input type="hidden" name="mname" class="form-control" value="<?php echo $userdata['mname'] ?>">
                <input type="hidden" name="contactno" class="form-control" value="<?php echo $userdata['contactno'] ?>">
                <input type="hidden" name="address" class="form-control" value="<?php echo $userdata['address'] ?>">

                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                    Which medicine or product are you interested in?
                    </div>
                    <div class="col-8">
                        <select class="form-control select" name="medicine">
                            <option value="">-- Select --</option>
                            <option value="Fiasp">Fiasp</option>
                            <option value="Levemir">Levemir</option>
                            <option value="Dont Know">Dont Know</option>
                        </select>
                        <span id="medicine_error" class="text-danger small"><?php echo form_error('medicine'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Current Doctor Name
                    </div>
                    <div class="col-8">
                        <input type="text" name="doctor" class="form-control" placeholder="Current Doctor Name">
                        <span id="doctor_error" class="text-danger small"><?php echo form_error('doctor'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        City
                    </div>
                    <div class="col-8">
                        <input type="text" name="city" class="form-control" placeholder="City">
                        <span id="city_error" class="text-danger small"><?php echo form_error('city'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-12 text-right">
                        <input type="reset" class="btn btn-danger" value="Reset"/>
                        <input type="submit" class="btn btn-success" value="Register" name="register"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script><script type="text/javascript">
    $('#datepicker').datepicker({
        weekStart: 1,
        daysOfWeekHighlighted: "6,0",
        autoclose: true,
        todayHighlight: true,
    });
    $('#datepicker').datepicker("setDate", new Date());
</script>
</body>