<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Registration Demo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style type="text/css">
        .emptyrow{
            height: 20px;
            width: 100%;
        }
        .datepicker td, .datepicker th {
            width: 2em;
            height: 2em;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h1>Registration Form</h1>
        </div>
    </div>
    <div class="emptyrow"></div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <form action="<?= base_url(); ?>personal" method="POST">
                <div class="row">
                    <div class="col-4 text-right">
                        I am
                    </div>
                    <div class="col-8">
                        <label><input type="radio" name="usertype" value="Patient or caregiver"> Patient or caregiver</label>
                        <label><input type="radio" name="usertype" value="Health care professional"> Health care professional</label>
                        <span id="usertype_error" class="text-danger small"><?php echo form_error('usertype'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Gender
                    </div>
                    <div class="col-8">
                        <label><input type="radio" name="gender" value="male"> Male</label>
                        <label><input type="radio" name="gender" value="female"> Female</label>
                        <span id="gender_error" class="text-danger small"><?php echo form_error('gender'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Being treated for
                    </div>
                    <div class="col-8">
                        <select class="form-control select" name="treatment">
                            <option value="">-- Select --</option>
                            <option value="Diabaties">Diabaties</option>
                            <option value="BP">BP</option>
                            <option value="General">General</option>
                        </select>
                        <span id="treatment_error" class="text-danger small"><?php echo form_error('treatment'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Name
                    </div>
                    <div class="col-8">
                    <input type="text" placeholder="Full Name" class="form-control" name="fullname">
                    <span id="fullname_error" class="text-danger small"><?php echo form_error('fullname'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Date of Birth
                    </div>
                    <div class="col-8">
                    <!-- <input data-date-format="dd/mm/yyyy" id="datepicker" class="form-control" name="dob"> -->
                    <input class="form-control" name="dob" placeholder="DD/MM/YYYY"/>
                    <span id="dob_error" class="text-danger small"><?php echo form_error('dob'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-12 text-right">
                        <input type="reset" class="btn btn-danger" value="Reset" name='reset'/>
                        <input type="submit" class="btn btn-success" value="Next-> Personal Details" name='submit'/>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script><script type="text/javascript">
    $('#datepicker').datepicker({
        weekStart: 1,
        daysOfWeekHighlighted: "6,0",
        autoclose: true,
        todayHighlight: true,
    });
    $('#datepicker').datepicker("setDate", new Date());
</script>
</body>