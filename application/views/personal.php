<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Welcome to CodeIgniter</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style type="text/css">
        .emptyrow{
            height: 20px;
            width: 100%;
        }
        .datepicker td, .datepicker th {
            width: 2em;
            height: 2em;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h1>Personal Details</h1>
        </div>
    </div>
    <div class="emptyrow"></div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <form action="<?= base_url(); ?>medical" method="POST">
                <div class="row">
                    <div class="col-4 text-right">
                        Full Name: 
                    </div>
                    <div class="col-8">
                        <input type="text" name="fullname" class="form-control" value="<?php echo $userdata['fullname'] ?>" readonly>
                    </div>
                </div>
                <div class="emptyrow"></div>

                <input type="hidden" name="gender" class="form-control" value="<?php echo $userdata['gender'] ?>">           
                <input type="hidden" name="treatment" class="form-control" value="<?php echo $userdata['treatment'] ?>">                
                <input type="hidden" name="usertype" class="form-control" value="<?php echo $userdata['usertype'] ?>">                

                <div class="row">
                    <div class="col-4 text-right">
                        Date of Birth
                    </div>
                    <div class="col-8">
                    <input type="text" name="dob" class="form-control" value="<?php echo $userdata['dob'] ?>" readonly>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Father Name
                    </div>
                    <div class="col-8">
                    <input type="text" name="fname" class="form-control" placeholder="Father Name">
                    <span id="fname_error" class="text-danger small"><?php echo form_error('fname'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Mother Name
                    </div>
                    <div class="col-8">
                    <input type="text" name="mname" class="form-control" placeholder="Mother Name">
                    <span id="mname_error" class="text-danger small"><?php echo form_error('mname'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Contact Number
                    </div>
                    <div class="col-8">
                    <input type="text" name="contactno" class="form-control" placeholder="10 Digits | Contact Number | Don't add country code">
                    <span id="contactno_error" class="text-danger small"><?php echo form_error('contactno'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-4 text-right">
                        Address
                    </div>
                    <div class="col-8">
                    <input type="text" name="address" class="form-control" placeholder="Current Address">
                    <span id="address_error" class="text-danger small"><?php echo form_error('address'); ?></span>
                    </div>
                </div>
                <div class="emptyrow"></div>
                <div class="row">
                    <div class="col-12 text-right">
                        <input type="reset" class="btn btn-danger" value="Reset"/>
                        <input type="submit" class="btn btn-success" value="Next -> Medical Records" name="submit"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-2"></div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script><script type="text/javascript">
    $('#datepicker').datepicker({
        weekStart: 1,
        daysOfWeekHighlighted: "6,0",
        autoclose: true,
        todayHighlight: true,
    });
    $('#datepicker').datepicker("setDate", new Date());
</script>
</body>